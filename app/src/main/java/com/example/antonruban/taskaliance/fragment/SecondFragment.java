package com.example.antonruban.taskaliance.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.antonruban.taskaliance.R;
import com.example.antonruban.taskaliance.activity.ThirdActivity;
import com.example.antonruban.taskaliance.adapter.SecondAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;

/**
 @author antonruban on 11.04.2018.
 */

public class SecondFragment extends Fragment {

    private InterstitialAd interstitialAd;
    private Button button4;
    private  RecyclerView recyclerView;
    private SecondAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_second, container, false);
        button4 = view.findViewById(R.id.button4);
        recyclerView = view.findViewById(R.id.recyclerView);

        ArrayList<String> animalNames = new ArrayList<>();
        animalNames.add(getResources().getString(R.string.button1));
        animalNames.add(getResources().getString(R.string.button2));
        animalNames.add(getResources().getString(R.string.button3));
        animalNames.add(getResources().getString(R.string.button5));
        animalNames.add(getResources().getString(R.string.button6));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SecondAdapter(getActivity(), animalNames);
        recyclerView.setAdapter(adapter);

        AdRequest adRequest = new AdRequest.Builder().build();
        MobileAds.initialize(getActivity(),"ca-app-pub-1306966969595152~8179477505");
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        interstitialAd.loadAd(adRequest);
        interstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                display();
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.addToBackStack(null);
                startActivity(new Intent(getActivity(), ThirdActivity.class));
            }
        });
        return view;
    }

    public void display() {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }
}
