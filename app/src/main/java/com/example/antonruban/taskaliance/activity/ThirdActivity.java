package com.example.antonruban.taskaliance.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.antonruban.taskaliance.interfaceView.IThirdView;
import com.example.antonruban.taskaliance.R;
import com.example.antonruban.taskaliance.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class ThirdActivity extends AppCompatActivity implements IThirdView{

    private InterstitialAd interstitialAd;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        AdRequest adRequest = new AdRequest.Builder().build();
        MobileAds.initialize(this,"ca-app-pub-1306966969595152~8179477505");
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        interstitialAd.loadAd(adRequest);
        interstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                display();
            }
        });

        mAdView = findViewById(R.id.adView);
        mAdView.loadAd(adRequest);
    }

    public void display() {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }

    public void onClickDownload(View view) {
        Utils.createDir(Environment.getExternalStorageDirectory().toString(),"Downloads");
        Utils.createDir(Environment.getExternalStorageDirectory().toString()+"/Downloads", "Files");

        String unzipLocation = Environment.getExternalStorageDirectory() +"/"+"Downloads"+"/"+"Files"+"/";
        String zipFile =Environment.getExternalStorageDirectory() +"/"+"Downloads"+"/"+"Files"+"."+"zip";
        String url="https://github.com/RubanAnton/stb34-101-77/archive/master.zip";
        try {
            new Utils(this).downloadEventData(ThirdActivity.this,zipFile, unzipLocation, url);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    @Override
    public void rewardedVideo() {
        startActivity(new Intent(this,FourthActivity.class));
    }

    @Override
    public String waitPlease() {
        return getResources().getString(R.string.zip);
    }

    public void onClickShare(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ThirdActivity.this);
        builder.setTitle(R.string.button5)
                .setCancelable(false)
                .setNegativeButton(R.string.button5,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClickVote(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ThirdActivity.this);
        builder.setTitle(R.string.button6)
                .setCancelable(false)
                .setNegativeButton(R.string.button7,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent email = new Intent(Intent.ACTION_SEND);
                                email.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.email)});
                                email.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.subject));
                                email.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.textBad));
                                email.setType("message/rfc822");
                                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                            }
                        });
        builder.setTitle(R.string.button6)
                .setCancelable(false)
                .setPositiveButton(R.string.button8,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent email = new Intent(Intent.ACTION_SEND);
                                email.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.email)});
                                email.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.subject));
                                email.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.textOk));
                                email.setType("message/rfc822");
                                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClickPromo(View view) {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
