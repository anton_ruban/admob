package com.example.antonruban.taskaliance.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.antonruban.taskaliance.R;
import com.example.antonruban.taskaliance.fragment.FirstFragment;

public class MainActivity extends AppCompatActivity {

    private FirstFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment = new FirstFragment();
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();


    }

    public void onClickShare(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.button5)
                .setCancelable(false)
                .setNegativeButton(R.string.button5,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClickVote(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.button6)
                .setCancelable(false)
                .setNegativeButton(R.string.button7,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent email = new Intent(Intent.ACTION_SEND);
                                email.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.email)});
                                email.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.subject));
                                email.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.textBad));
                                email.setType("message/rfc822");
                                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                            }
                        });
        builder.setTitle(R.string.button6)
                .setCancelable(false)
                .setPositiveButton(R.string.button8,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent email = new Intent(Intent.ACTION_SEND);
                                email.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.email)});
                                email.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.subject));
                                email.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.textOk));
                                email.setType("message/rfc822");
                                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClickPromo(View view) {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
