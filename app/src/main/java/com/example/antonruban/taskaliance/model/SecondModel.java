package com.example.antonruban.taskaliance.model;

/**
 @author antonruban on 11.04.2018.
 */

public class SecondModel {
    String name;

    public SecondModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
