package com.example.antonruban.taskaliance.interfaceView;

/**
 @author antonruban on 11.04.2018.
 */

public interface IThirdView {

    void rewardedVideo();
    String waitPlease();
}
