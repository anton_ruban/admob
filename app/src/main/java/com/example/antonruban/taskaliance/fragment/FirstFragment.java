package com.example.antonruban.taskaliance.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.antonruban.taskaliance.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

/**
 @author antonruban on 11.04.2018.
 */

public class FirstFragment extends Fragment {

    private InterstitialAd interstitialAd;
    private Button button1;
    private SecondFragment fragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_first, container, false);
        button1 = view.findViewById(R.id.button1);
        final FragmentManager manager = getFragmentManager();

        AdRequest adRequest = new AdRequest.Builder().build();
        MobileAds.initialize(getActivity(),"ca-app-pub-1306966969595152~8179477505");
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        interstitialAd.loadAd(adRequest);
        interstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                display();
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new SecondFragment();

                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.container,fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    public void display() {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }
}
